#include <stdio.h>

int main(){
  srand(time(NULL));
  int life=1,score=0;
  do{
    int a=rand()%9+1;
    int b=rand()%9+1;
    int res;
    printf("%d×%d=",a,b);
    scanf("%d",&res);
    if(res==a*b){
      printf("정답!\n");
      score++;
    }else{
      printf("오답 ㅠㅠ\n");
      life--;
    }
  }while(life);
  printf("당신은 무려 %d 문제나 맞췄어요! 축하해요!",score);
}
