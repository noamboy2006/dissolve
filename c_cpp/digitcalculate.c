#include <stdio.h>

int multiply(int x,int y);
int divide(int x,int y);

int main(){
  int a,b;
  printf("정수 2개를 입력해주세요.\n");
  scanf("%d %d",&a,&b);
  if(b>a){
    int temp=a;
    a=b;
    b=temp;
  }
  printf("%d곱하기 %d=%d\n %d나누기 %d=%d\n",multiply(a,b),divide(a,b));
  return 0;
}

int multiply(int x,int y){
  int i,res=0,ysign=1;
  if(y<0)
    ysign=-1;
  for(i=0;i<ysign*y;i++){
    res+=ysign*x;
  }
  return res;
}

int divide(int x,int y){
  int i,temp=0,xsign=1,ysign=1;
  if(x<0)
    xsign=-1;
    if(y<0)
      ysign=-1;
    temp=xsign*x;
  for(i=0;temp>0;i++){
    temp-=ysign*y;
  }
  if(temp)i-=1*ysign;
  return xsign*ysign*i;
}
